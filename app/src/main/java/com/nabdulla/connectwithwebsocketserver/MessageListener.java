package com.nabdulla.connectwithwebsocketserver;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;

public class MessageListener extends WebSocketListener implements Subject {
    private List<Observer> observers;

    public MessageListener() {
        this(new ArrayList<>());
    }

    public MessageListener(List<Observer> observers) {
        this.observers = observers;
    }

    @Override
    public synchronized boolean subscribe(Observer observer) {
        return observers.add(observer);
    }

    @Override
    public boolean remove(Observer observer) {
        return observers.remove(observer);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        super.onOpen(webSocket, response);
        Log.d("mytag", "connection opened.");
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        Message message = GsonSingleton.INSTANCE.get().fromJson(text, Message.class);
        for (Observer observer : observers) {
            observer.gotMessage(webSocket, message);
        }
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        super.onClosing(webSocket, code, reason);
        Log.d("mytag", "closing connection...");
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        super.onClosed(webSocket, code, reason);
        Log.d("mytag", "connection closed.");
    }
}
