package com.nabdulla.connectwithwebsocketserver;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import okhttp3.WebSocket;

public class MainActivity extends AppCompatActivity implements Observer {
    private EditText tv;
    private Button send;
    private EditText messageBox;
    private boolean connectionDropped;
    private boolean running;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.info);
        send = findViewById(R.id.send);
        messageBox = findViewById(R.id.messageBox);

        connectionDropped = true;
        running = false;
        if (NetworkStates.isConnected(getApplicationContext())) {
            ConnectionToServer.get().getMessageListener().subscribe(this);
            connectionDropped = false;
            running = true;
        } else {
            Toast.makeText(this, "Can't connect to internet", Toast.LENGTH_LONG).show();
            finish();
        }

        send.setOnClickListener(this::buttonClicked);
    }

    @Override
    public void gotMessage(WebSocket webSocket, Message msg) {
        String json = msg.getJson();
        //todo parse json
        runOnUiThread(() -> tv.setText(tv.getText() + "\n" + json));
    }

    @Override
    protected void onDestroy() {
        if (running) {
            ConnectionToServer.get().interrupt();
            ConnectionToServer.get().setExitNow(true);
        }
        super.onDestroy();
    }

    private void buttonClicked(View view) {
        if (messageBox.getText().length() != 0) {
            if (NetworkStates.isConnected(getApplicationContext())) {
                tv.setText(tv.getText() + "\n\n" + String.format("me: %s", messageBox.getText().toString()));
                if (connectionDropped) {
                    ConnectionToServer.get().getMessageListener().subscribe(this);
                    connectionDropped = false;
                    running = true;
                }
                ConnectionToServer.get().sendMessage(messageBox.getText().toString());
                messageBox.getText().clear();
            } else {
                if (!connectionDropped) {
                    ConnectionToServer.get().interrupt();
                    ConnectionToServer.get().setExitNow(true);
                    connectionDropped = true;
                    running = false;
                }
                Toast.makeText(this, "Can't connect to internet", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

