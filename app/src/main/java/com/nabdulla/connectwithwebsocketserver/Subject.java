package com.nabdulla.connectwithwebsocketserver;

public interface Subject {
    boolean subscribe(Observer observer);

    boolean remove(Observer observer);
}
