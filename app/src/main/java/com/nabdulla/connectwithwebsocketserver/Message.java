package com.nabdulla.connectwithwebsocketserver;

import com.google.gson.annotations.SerializedName;

public class Message {
    @SerializedName("message")
    private String json;
    public Message(String msg){
        this.json = msg;
    }

    public String getJson() {
        return json;
    }
}
