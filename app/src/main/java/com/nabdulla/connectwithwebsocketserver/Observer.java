package com.nabdulla.connectwithwebsocketserver;

import okhttp3.WebSocket;

public interface Observer {
    void gotMessage(WebSocket webSocket, Message message);
}
