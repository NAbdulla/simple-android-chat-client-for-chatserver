package com.nabdulla.connectwithwebsocketserver;

import android.util.Log;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class ConnectionToServer extends Thread {
    private BlockingQueue<String> queue;
    private OkHttpClient client;
    private static ConnectionToServer connection;
    private final String SERVER_END = "wss://e33fcb1f.ngrok.io/ws/chat/app/";
    private MessageListener messageListener;
    private boolean exitNow;

    private ConnectionToServer() {
        super.setName("Server Connection Thread");
        client = new OkHttpClient();
        queue = new LinkedBlockingQueue<>();
        messageListener = new MessageListener();
        exitNow = false;
    }

    public static ConnectionToServer get() {
        if (connection == null || !connection.isAlive()) {
            synchronized (ConnectionToServer.class) {
                if (connection == null || !connection.isAlive()) {
                    connection = new ConnectionToServer();
                    connection.start();
                }
            }
        }
        return connection;
    }

    public void sendMessage(String msg) {
        queue.offer(msg);
    }

    public MessageListener getMessageListener() {
        return messageListener;
    }

    public void setExitNow(boolean exitNow) {
        this.exitNow = exitNow;
    }

    @Override
    public void run() {
        Log.d("mytag", "connection running...");
        Request request = new Request.Builder()
                .url(SERVER_END)
                .build();
        WebSocket ws = client.newWebSocket(request, messageListener);
        while (!Thread.currentThread().isInterrupted() && !exitNow) {
            try {
                String data = queue.take();
                Message message = new Message(data);
                ws.send(GsonSingleton.INSTANCE.get().toJson(message));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
//                throw new AssertionError(e);
            }
        }
        Log.d("mytag", "closing connection and exiting...");
    }
}
