package com.nabdulla.connectwithwebsocketserver;

import com.google.gson.Gson;

public enum GsonSingleton {
    INSTANCE;
    private Gson gson;

    GsonSingleton() {
        this.gson = new Gson();
    }

    public Gson get() {
        return gson;
    }
}
